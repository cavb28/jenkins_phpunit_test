<?php

require 'GumballMachine.php';


class GumballMachineTest extends PHPUnit_Framework_TestCase{
    private $gumballMachineInstance;

    //Get the amount
    public function setUp() {
        $this->gumballMachineInstance = new GumballMachine();
    }

    //Set the amount
    public function testIfWheelworks() {
        $this->gumballMachineInstance->setGumballs(100);
        $this->gumballMachineInstance->turnWheel();
        $this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
    
    }
}