<?php

class GumballMachine{
    private $gumballs;

    //Get the amount 
    public function getGumballs() {
        return $this->gumballs;
    }

    //Set the amount
    public function setGumballs($amount) {
        $this->gumballs = $amount;
    }

    //The user turns the wheel to modify gumballs
    public function turnWheel() {
        $this->setGumballs($this->getGumballs() - 1);
    }
}